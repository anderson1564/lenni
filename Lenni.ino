//Delay with millis time
#define DELAY_TIME_SHORT 3000l
#define DELAY_TIME_LONG 5000
//Pulser pins
#define PUL_1 7
#define PUL_2 8
#define PUL_3 12
//RGB pins
#define PIN_R 3
#define PIN_G 5
#define PIN_B 6

//Value pulser
int pul_1 = 0;
int pul_2 = 0;
int pul_3 = 0;

void setup() 
{
  //Set pins mode
  pinMode(PUL_1, INPUT);
  pinMode(PUL_2, INPUT);
  pinMode(PUL_3, INPUT);
}

//Delay time in millis
void delay_millis(unsigned long time_millis)
{
  unsigned int end_time = millis() + time_millis;
  while (millis() < end_time);
}

//Change the color of the LED
void change_color(int r, int g, int b)
{
  analogWrite(PIN_R, r);
  analogWrite(PIN_G, g);
  analogWrite(PIN_B, b);
}

//Get the state of the push pulsers
int get_state()
{
  int state = 0;
  if (pul_1 == HIGH) {
    state += 2;
  }
  if (pul_2 == HIGH) {
    state += 4;
  }
  if (pul_3 == HIGH) {
    state += 5;
  }
  return state;
}

void loop() 
{  
  //Reset the state
  int state=0;

  //Update the pulsers state
  pul_1 = digitalRead(PUL_1);
  pul_2 = digitalRead(PUL_2);
  pul_3 = digitalRead(PUL_3);
  
  if (pul_1 == HIGH || pul_2 == HIGH ||  pul_3 == HIGH)
  {
    delay_millis(DELAY_TIME_SHORT);
    state = get_state();
  } 

  //Change the color of the LED and wait
  if (state) 
  {
    switch (state) 
    {
      case 2: change_color(0, 255, 0);
              break;
      case 4: change_color(208, 168, 24);
              break;
      case 5: change_color(3, 5, 117);
              break;
      case 6: change_color(255, 0, 0);
              break;
      case 7: change_color(237, 92, 6);
              break;
      case 9: change_color(125, 31, 122);
              break;
      case 11:  change_color(0, 0, 255);
                break;
      default:  delay(DELAY_TIME_LONG);
    }
  }
  change_color(0, 0, 0);
}